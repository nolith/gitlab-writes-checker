PKG := gitlab.com/nolith/gitlab-writes-checker
PKGS := $(shell go list $(PKG)/...)

BINARY := gitlab-writes-checker
VERSION ?= $(shell git describe --tag --always)
PLATFORMS := linux darwin
os = $(word 1, $@)

all: build

build: $(BINARY)

$(BINARY):
	go build $(PKG)

.PHONY: test
test:
	go test -race $(PKGS)

.PHONY: fmt
fmt:
	go fmt $(PKGS)

.PHONY: vet
vet:
	go vet $(PKGS)

.PHONY: $(PLATFORMS)
$(PLATFORMS):
	@mkdir -p release
	GOOS=$(os) GOARCH=amd64 go build -o release/$(BINARY)-$(VERSION)-$(os)-amd64 $(PKG)

.PHONY: release
release: linux darwin
