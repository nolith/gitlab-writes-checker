module gitlab.com/nolith/gitlab-writes-checker

go 1.12

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/karrick/godirwalk v1.10.12
	github.com/stretchr/testify v1.3.0
	gitlab.com/gitlab-org/labkit v0.0.0-20190902063225-3253d7975ca7
)
