package tracker

import (
	"context"
	"os"
	"path/filepath"
	"sync"

	"github.com/fsnotify/fsnotify"
	"github.com/karrick/godirwalk"
	"gitlab.com/gitlab-org/labkit/log"
)

type Watcher interface {
	Events() <-chan Event
	Errors() <-chan error
}

type EventName string

const (
	Create EventName = "create"
	Write  EventName = "write"
	Remove EventName = "remove"
	Found  EventName = "found"
)

type Event struct {
	Name EventName
	Path string
}

type watcher struct {
	watcher *fsnotify.Watcher
	events  chan Event

	ctx    context.Context
	cancel context.CancelFunc

	mu      sync.Mutex
	watches map[string]bool // key: path
}

func (w *watcher) Events() <-chan Event {
	return w.events
}

func (w *watcher) Errors() <-chan error {
	return w.watcher.Errors
}

func (w *watcher) processEvent(event fsnotify.Event) {
	log.WithField("event", event.String()).Debug("event received")
	path := event.Name

	if event.Op&fsnotify.Create == fsnotify.Create {
		w.events <- Event{Path: event.Name, Name: Create}

		fi, err := os.Lstat(path)
		if err != nil {
			log.WithError(err).WithField("path", path).Error("Cannot lstat")
		} else if fi.IsDir() {
			if err := w.walkIntoPath(path); err != nil {
				log.WithError(err).WithField("path", path).Error("Cannot walk into path")
			}
		}
	}
	if event.Op&fsnotify.Write == fsnotify.Write {
		w.events <- Event{Path: event.Name, Name: Write}
	}
	if event.Op&fsnotify.Remove == fsnotify.Remove {
		go w.remove(path)

		w.events <- Event{Path: event.Name, Name: Remove}
	}
}

func (w *watcher) eventsLoop() {
	log.WithField("action", "start").Debug("Watcher events loop")
	done := false
	for !done {
		select {
		case event, ok := <-w.watcher.Events:
			if !ok {
				return
			}

			w.processEvent(event)

		case <-w.ctx.Done():
			w.watcher.Close()
			w.cancel()
			done = true
		}
	}
	log.WithField("action", "stop").Debug("Watcher events loop")
}

func (w *watcher) walkDirFn() filepath.WalkFunc {
	return func(path string, info os.FileInfo, err error) error {
		w.found(path)

		// walk only directories
		if !info.IsDir() {
			return nil
		}
		log.WithField("path", path).Debug("directory walk")

		if err != nil {
			log.WithError(err).WithField("path", path).Error("directory walk")
			return err
		}

		return w.add(path)
	}
}

func New(ctx context.Context, path string) (Watcher, error) {
	internalWatcher, err := fsnotify.NewWatcher()
	if err != nil {
		return nil, err
	}

	ctx2, cancel := context.WithCancel(ctx)
	w := &watcher{
		watcher: internalWatcher,
		events:  make(chan Event),
		ctx:     ctx2,
		cancel:  cancel,
		mu:      sync.Mutex{},
		watches: make(map[string]bool),
	}

	go w.eventsLoop()

	return w, w.walkIntoPath(path)
}

func (w *watcher) add(path string) error {
	w.mu.Lock()
	w.watches[path] = true
	w.mu.Unlock()

	err := w.watcher.Add(path)
	if err != nil {
		log.WithError(err).WithField("path", path).Error("Cannot watch path")
		return err
	}

	log.WithField("path", path).Debug("Add watch")

	return nil
}

func (w *watcher) remove(path string) error {
	w.mu.Lock()
	//defer w.mu.Unlock()
	defer w.mu.Unlock()

	if tracked, _ := w.watches[path]; tracked {
		delete(w.watches, path)

		err := w.watcher.Remove(path)
		if err != nil {
			log.WithError(err).WithField("path", path).Error("Cannot unwatch path")
		}

		return err
	}

	return nil
}

func (w *watcher) walkIntoPath(path string) error {
	if rootError := w.add(path); rootError != nil {
		return rootError
	}

	return godirwalk.Walk(path, &godirwalk.Options{
		Callback: func(osPathname string, info *godirwalk.Dirent) error {
			w.found(osPathname)

			// walk only directories
			if !info.IsDir() {
				return nil
			}

			log.WithField("path", osPathname).Debug("directory walk")

			return w.add(osPathname)
		},
		ErrorCallback: func(osPathname string, err error) godirwalk.ErrorAction {
			log.WithError(err).WithField("path", osPathname).Error("directory walk")
			return godirwalk.SkipNode
		},
		Unsorted: true, // set true for faster yet non-deterministic enumeration (see godoc)
	})
	//return filepath.Walk(path, w.walkDirFn())
}

func (w *watcher) found(path string) {
	if w.isWatched(path) {
		return
	}

	go func() {
		w.events <- Event{Path: path, Name: Found}
	}()
}

func (w *watcher) isWatched(path string) bool {
	w.mu.Lock()
	defer w.mu.Unlock()

	tracked, _ := w.watches[path]
	return tracked
}
