package tracker

import (
	"context"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/labkit/log"
)

const fetchTimeout time.Duration = 10 * time.Second

func initLogger() error {
	_, err := log.Initialize(
		log.WithFormatter("color"),
		log.WithLogLevel("debug"),
	)

	return err
}

func fetchEvent(t *testing.T, w Watcher) Event {
	t.Helper()
	var event Event

	select {
	case <-time.After(fetchTimeout):
		t.Fatal("Event wait timeout")
	case e := <-w.Events():
		log.WithFields(log.Fields{"path": e.Path, "event": e.Name}).Info("event fetched")
		event = e
	case err := <-w.Errors():
		t.Fatal("Error ", err)
	}

	return event
}

func TestFileEvents(t *testing.T) {
	t.Skip("Not working on linux")
	tree := "sub1/sub2"
	fileNames := []string{
		"test",
		"sub1/test",
		"sub1/sub2/test",
	}
	events := []Event{
		Event{Found, "sub1"},
		Event{Found, "sub1/sub2"},
	}

	for _, fileName := range fileNames {
		t.Run(fileName, func(t *testing.T) {
			require := require.New(t)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			tmpDir, err := ioutil.TempDir("", "gitlab-writes-checker")
			require.NoError(err)
			defer os.RemoveAll(tmpDir)
			require.NoError(os.MkdirAll(filepath.Join(tmpDir, tree), 0755))

			require.NoError(initLogger())

			w, err := New(ctx, tmpDir)
			require.NoError(err)

			expectEventSequence(t, w, tmpDir, events)

			filePath := filepath.Join(tmpDir, fileName)
			testFileLifecycle(t, filePath, w)

			cancel()
		})
	}
}

func testFileLifecycle(t *testing.T, filePath string, w Watcher) {
	require := require.New(t)

	// create
	file, err := os.Create(filePath)
	require.NoError(err)
	defer file.Close()

	e1 := fetchEvent(t, w)
	require.Equal(filePath, e1.Path)
	require.Equal(Create, e1.Name)

	// write
	_, err = file.WriteString("hello")
	require.NoError(err)
	require.NoError(file.Close())

	e2 := fetchEvent(t, w)
	require.Equal(filePath, e2.Path)
	require.Equal(Write, e2.Name)

	// remove
	os.Remove(filePath)

	e3 := fetchEvent(t, w)
	require.Equal(filePath, e3.Path)
	require.Equal(Remove, e3.Name)
}

func TestWatchNewDirectories(t *testing.T) {
	t.Skip("Not working on linux")
	tree := "sub1/sub2"
	fileNames := []string{
		"sub1/test",
		"sub1/sub2/test",
	}
	events := []Event{
		Event{Create, "sub1"},
		Event{Found, "sub1/sub2"},
	}

	for _, fileName := range fileNames {
		t.Run(fileName, func(t *testing.T) {
			require := require.New(t)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			tmpDir, err := ioutil.TempDir("", "gitlab-writes-checker")
			require.NoError(err)
			defer os.RemoveAll(tmpDir)

			require.NoError(initLogger())

			w, err := New(ctx, tmpDir)
			require.NoError(err)

			// this will take time allowing us to sync expected events
			testFileLifecycle(t, filepath.Join(tmpDir, "sync"), w)

			require.NoError(os.MkdirAll(filepath.Join(tmpDir, tree), 0755))
			expectEventSequence(t, w, tmpDir, events)

			// walking the fs can be slow. here we inject some events in order to ensure
			// the walk is completed before we create a file inside a nested directory
			testFileLifecycle(t, filepath.Join(tmpDir, "sync"), w)

			filePath := path.Join(tmpDir, fileName)
			testFileLifecycle(t, filePath, w)

			cancel()
		})
	}
}

func expectEventSequence(t *testing.T, w Watcher, tmpDir string, events []Event) {
	t.Helper()
	timeout := time.After(fetchTimeout)

	for _, expected := range events {
		expected.Path = filepath.Join(tmpDir, expected.Path)
		log.WithFields(log.Fields{"path": expected.Path, "name": expected.Name}).Info("Waiting event")

		select {
		case event := <-w.Events():
			log.WithFields(log.Fields{"path": event.Path, "name": event.Name}).Info("Fetching event")
			require.Equal(t, expected, event)
		case <-timeout:
			t.Fatal("Event timeout")
		}
	}
}
