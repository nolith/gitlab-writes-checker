package main

import (
	"context"
	"flag"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/gitlab-org/labkit/log"

	"gitlab.com/nolith/gitlab-writes-checker/pkg/tracker"
)

var format = flag.String("log-format", "color", "the logging format")
var debug = flag.Bool("v", false, "enable debug logging")

func main() {
	flag.Parse()

	path := flag.Arg(0)
	logLevel := "info"
	if *debug {
		logLevel = "debug"
	}

	closer, err := log.Initialize(
		log.WithFormatter(*format),
		log.WithLogLevel(logLevel),
	)
	if err != nil {
		log.WithError(err).Error("init log system")
	}
	defer closer.Close()

	log.WithFields(log.Fields{"debug": *debug, "log-level": logLevel, "path": path}).Info("application start")

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	watcher, err := tracker.New(ctx, path)
	if err != nil {
		log.WithError(err).Fatal("tracker init")
	}

	n := make(chan os.Signal, 1)
	signal.Notify(n, syscall.SIGINT)
	go func() {
		for sig := range n {
			log.WithField("signal", sig).Error("signal trapped")
			cancel()
		}
	}()

	go func() {
		log.WithField("state", "start").Info("main loop")
		done := false
		for !done {
			select {
			case event := <-watcher.Events():
				log.WithFields(log.Fields{"name": event.Name, "path": event.Path}).Warn("disk operation")
			case err := <-watcher.Errors():
				log.WithError(err).Error("main loop")

			case <-ctx.Done():
				log.WithError(ctx.Err()).Debug("context done")
				done = true
			}
		}
		log.WithField("state", "stop").Info("main loop")
	}()

	<-ctx.Done()
	log.Info("bye")
}
